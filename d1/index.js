console.log("Hello World")

function printName(){
   console.log("My names is John");
}

printName();



function declaredFunction(){
   console.log("Hello from declaredFunction");
}
declaredFunction();



//FUNCTION EXPRESSION


let variableFunction = function(){
   console.log("Hello Again!");
}

variableFunction();



function animeLIST(){
   console.log("Naruto, One Piece, Dragon Ball Z");
 
}

function movieLIST(){
   console.log("Avengers, Mission Impossible, John Wick");
}

animeLIST();
movieLIST();


//Reassigning declared functions


declaredFunction = function(){
   console.log("updated declaredFunction");
}
declaredFunction();


const constFunction = function(){
   console.log("Initialize const function");
}
constFunction();


//Function Scoping
//local scope
{
   let localVar = "Armando Perez";
   console.log(localVar);
}
//global scope
let globalVar = "Mr. Worldwide";
console.log(globalVar);


function showNames(){
   var functionVar = "Joe";
   const functionConst = "John";
   let functionLet = "Jane";
   console.log(functionVar);
   console.log(functionConst);
   console.log(functionLet);
}
showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

//Nested Functions
function newFunctions(){
   let named = "Jane";

   function nestedFunction(){
      let nestedName = "John";
       console.log(named);
      console.log(nestedName);
   }
   nestedFunction();
}
newFunctions();



//global variable
let globalName = "Alex";

function myNewFunction(){
   let nameInside = "Renz";
   console.log(globalName);
   console.log(nameInside);
}
myNewFunction();

//using alert()
//alert() - allows us to show a small window at the top of our browser to show information to our users. As opposed to a console.log which only shows the message on the console. It allows us to show a short dialog or instructions to our user. The page will continue to load until the user dismisses the dialog.

alert("Hello world");


//we can use an alert() to show message to the user from a later function invocation
//------------------------------------------
function showSampleAlert(){
   alert("Hello Again");
}
showSampleAlert();

console.log("I will be displayed after the alert has been closed");

/*
   Notes on using alert()
      show only an alert() for short dialogs/messages to the user.
*/

//using prompt()---------------------------

let samplePrompt = prompt("Enter your name.");

console.log(typeof samplePrompt);
console.log("Hello " + samplePrompt);

// let nullPrompt = prompt("do not enter anything here");
// console.log(nullPrompt);-------------

function welcomeMessage(){
   let firstName = prompt("enter your first name: ");
   let lastName = prompt("enter your last name: ");
   console.log("Hello: " + firstName + " " + lastName);
}
welcomeMessage();

//Function Naming

function getCourses(){
   let courses = ["Science 101","Math 101","English 101",];
   console.log(courses);
}
getCourses();

function get(){
   let name = "Jamie";
   console.log(name);
}
get();

function foo(){
   console.log(25%5);
}
foo();