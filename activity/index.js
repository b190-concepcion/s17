console.log("Hello World");

let fullName = prompt("What is your name? ");
let yourAge = prompt("How old are you? ");
let yourPlace = prompt("Where do you live?");
console.log("Hello " + fullName);
console.log("You are " + yourAge + " years old.");
console.log("You live in " + yourPlace);

function myFavBands(){
   console.log("1. Parokya Ni Edgar");
   console.log("2. Kamikazee");
   console.log("3. Siakol");
   console.log("4. West Life");
   console.log("5. Maroon 5");
}
myFavBands();


function myFavMovies(){
   console.log("1. Spiderhead");
   console.log("Rotten Tomatoes Rating: 41%");
   console.log("2. Doctor Strange");
   console.log("Rotten Tomatoes Rating: 74%");
   console.log("3. Hustle");
   console.log("Rotten Tomatoes Rating: 92%");
   console.log("4. Everything Everywhere All at Once");
   console.log("Rotten Tomatoes Rating: 95%");
   console.log("5. The Man From Toronto");
   console.log("Rotten Tomatoes Rating: 28%");
}
myFavMovies();

// printUsers();

let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/*
console.log(friend1);
console.log(friend2);*/

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

